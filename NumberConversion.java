public class NumberConversion{
    public static int romanToArabic(String romanNumeral){
	int arabicNumber = 0;
	for (int i=0; i<romanNumeral.length(); i++){
	    char nextChar = romanNumeral.charAt(i);
	    switch (nextChar) {
	    case 'M':
		arabicNumber+=1000;
	    case 'D':
		arabicNumber+=500;
	    case 'C':
		arabicNumber+=100;
	    case 'L':
		arabicNumber+=50;
	    case 'X':
	        arabicNumber+=10;
	    case 'V':
	        arabicNumber+=5;
	    case 'I':
	        arabicNumber+=1;
	    }
	}
	return arabicNumber;
    }
    public static String arabicToRoman(int arabicNumeral){
    String romanNumeral = "";
    while (arabicNumeral >=1000){
	arabicNumeral-=1000;
	romanNumeral = romanNumeral + "M";
    }
    while (arabicNumeral >=500){
	arabicNumeral-=500;
	romanNumeral = romanNumeral + "D";
    }
    while (arabicNumeral >=100){
	arabicNumeral-=100;
	romanNumeral = romanNumeral + "C";
    }
    while (arabicNumeral >=50){
	arabicNumeral-=50;
	romanNumeral = romanNumeral + "L";
    }
    while (arabicNumeral >=10){
	arabicNumeral-=10;
	romanNumeral = romanNumeral + "X";
    }
    while (arabicNumeral >=5){
	arabicNumeral-=5;
	romanNumeral = romanNumeral + "V";
    }
    while (arabicNumeral >=1){
	arabicNumeral-=1;
	romanNumeral = romanNumeral + "I";
    }
    return romanNumeral;
    }
}
